from django.utils.translation import gettext_lazy as _
from modeltranslation.translator import TranslationOptions
from modeltranslation.decorators import register
from .models import (
    CaptchaFormField,
    FormPage,
    FooterText,
    HomePage,
    HomePageFeatureRelationship,
    StyleSnippet,
    StandardPage,
    People,
    PeopleIndexPage,
)

# This is just so the makemessages command captures this
RECAPTCHA_MESSAGE = _("reCaptcha invalid or expired, try again")
SUBMIT = _("Submit")
BACKTOHOMEPAGE = _("Back to Home Page")


@register(People)
class PeopleTR(TranslationOptions):
    fields = ('job_title', 'bio')

@register(StyleSnippet)
class StyleSnippetTR(TranslationOptions):
    pass

@register(StandardPage)
class StandardPageTR(TranslationOptions):
    fields = ('introduction',)

@register(PeopleIndexPage)
class PeopleIndexPageTR(TranslationOptions):
    fields = ('introduction',)


@register(FooterText)
class FooterTextTR(TranslationOptions):
    fields = ('body',)


@register(FormPage)
class FormPageTR(TranslationOptions):
    fields = (
        'thank_you_text',
    )

@register(CaptchaFormField)
class CaptchaFormFieldTR(TranslationOptions):
    fields = ('label', 'placeholder',)


@register(HomePage)
class HomePageTR(TranslationOptions):
    fields = (
        'hero_text',
        'hero_cta',
        'promo_title',
        'promo_text',
    )


@register(HomePageFeatureRelationship)
class HomePageFeatureRelationshipTR(TranslationOptions):
    fields = (
        'title',
        'message',
    )
