"""
Generates a secret key and appends it to the project's
secrets.py
This standalone version is designed not to crash when
django.conf.settings cannot import the secrets file.
"""
import sys
from random import SystemRandom

def handle():
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    key = ''.join(SystemRandom().sample(chars, k=50))
    sys.stdout.write("%(key)s" % {
        'key': key,
    })

if __name__ == '__main__':
    handle()
