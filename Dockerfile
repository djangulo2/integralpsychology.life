FROM python:3.6
LABEL maintainer="denis.angulo@gmail.com"

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install --trusted-host pypi.python.org -r requirements.txt
ADD . /code/


RUN python manage.py migrate
RUN python manage.py update_index
RUN python manage.py sync_page_translation_fields
RUN python manage.py update_translation_fields

# RUN useradd wagtail
# RUN chown -R wagtail /code
# USER wagtail

EXPOSE 8000
CMD exec gunicorn integralpsychology.wsgi:application --bind 0.0.0.0:8000 --workers 3
